#+title: sandbox-k8s-consul-vault
#+options: num:nil toc:nil
#+property: header-args:sh :exports both

#+begin_src sh :exports both :results output
  echo "foobar"
#+end_src

* Summary

Sandbox for deploying Consul and Vault into various hosted k8s environments for testing Helm Chart output
against EA Reference Architectures.

* Usage

** GKE
*** Provision the k8s cluster
   1. Set up the Terraform Enterprise Workspace for provisioning of the k8s cluster:

   #+begin_src sh
     cd terraform/workspace
     terraform init && terraform plan && terraform apply -auto-approve
   #+end_src

   2. Provision the k8s cluster

       Login to TFE and Queue a provisioning run. Once it has completed, proceed.

   3. Configure kubectl to access the new cluster

   #+begin_src sh
     gcloud config set project demos-nvalentine
     gcloud container clusters list
     gcloud container clusters get-credentials hashi-demos
     kubectl cluster-info
   #+end_src

   #+RESULTS:

*** Install Helm (w/ Tiller) on the k8s cluster
    Newer versions of Helm may not require [[][Tiller]] and and in those situations there's no Helm setup. For versions
    of Helm which **do** require Tiller:

    1. Let's make sure Helm/Tiller is not already installed.
    #+begin_src sh
      kubectl get pods --namespace=kube-system | grep tiller
    #+end_src

    1. If Tiller is not installed, let's install it and validate proper operation.
    #+begin_src sh :prologue helm reset --force > /dev/null 2>&1
      set +x
      helm init
      sleep 5 # wait for download of Tiller image and spin of Tiller resources
      kubectl get pods --namespace=kube-system | grep tiller
      helm version
    #+end_src

    #+RESULTS:
    #+begin_example
    $HELM_HOME has been configured at /home/nrvale0/.helm.

    Tiller (the Helm server-side component) has been installed into your Kubernetes Cluster.

    Please note: by default, Tiller is deployed with an insecure 'allow unauthenticated users' policy.
    To prevent this, run `helm init` with the --tiller-tls-verify flag.
    For more information on securing your installation see: https://docs.helm.sh/using_helm/#securing-your-helm-installation
    Happy Helming!
    tiller-deploy-759b9d56c-x4t7c            1/1     Running   0          34m
    Client: &version.Version{SemVer:"v2.11.0", GitCommit:"2e55dbe1fdb5fdb96b75ff144a339489417b146b", GitTreeState:"clean"}
    Server: &version.Version{SemVer:"v2.11.0", GitCommit:"2e55dbe1fdb5fdb96b75ff144a339489417b146b", GitTreeState:"clean"}
    #+end_example

*** Download the Consul and Vault Helm Charts

  #+begin_src sh :prologue rm -rf consul-helm vault-helm
    (git clone git@github.com:hashicorp/consul-helm && cd consul-helm && git checkout v0.3.0 && git status)
    # (git clone git@github.com:hashicorp/vault-helm && cd vault-helm && git checkout v0.3.0 && git status) # FIXME: access to repo
  #+end_src

  #+RESULTS:
  : HEAD detached at v0.3.0
  : nothing to commit, working tree clean

*** Deploy Consul to GKE k8s

    Firt we instantiate a Consul Server cluster from the Helm Chart:

    #+begin_src sh
      helm install consul-helm \
	   --set ui.service.type=LoadBalancer \
	   --set connectInject.enabled=false  \
	   --set server.connect=false \
	   --wait
    #+end_src

    #+RESULTS:
    #+begin_example
    NAME:   belligerent-bat
    LAST DEPLOYED: Wed Nov 14 15:25:48 2018
    NAMESPACE: default
    STATUS: DEPLOYED

    RESOURCES:
    ==> v1/Service
    NAME                           AGE
    belligerent-bat-consul-dns     1m
    belligerent-bat-consul-server  1m
    belligerent-bat-consul-ui      1m

    ==> v1/DaemonSet
    belligerent-bat-consul  1m

    ==> v1/StatefulSet
    belligerent-bat-consul-server  1m

    ==> v1beta1/PodDisruptionBudget
    belligerent-bat-consul-server  1m

    ==> v1/Pod(related)

    NAME                             READY  STATUS   RESTARTS  AGE
    belligerent-bat-consul-66fn2     1/1    Running  0         1m
    belligerent-bat-consul-f7cnv     1/1    Running  0         1m
    belligerent-bat-consul-q5rbk     1/1    Running  0         1m
    belligerent-bat-consul-server-0  1/1    Running  0         1m
    belligerent-bat-consul-server-1  1/1    Running  0         1m
    belligerent-bat-consul-server-2  1/1    Running  0         1m

    ==> v1/ConfigMap

    NAME                                  AGE
    belligerent-bat-consul-client-config  1m
    belligerent-bat-consul-server-config  1m
    #+end_example

    Now let's get the address of the Consul cluster:

    #+begin_src sh
      kubectl get service | grep consul-ui
    #+end_src

    #+RESULTS:
    : belligerent-bat-consul-ui       LoadBalancer   10.59.245.133   35.233.141.160   80:30231/TCP                                                              4m1s


*** Deploy Vault to GKE k8s
    FIXME: write this
