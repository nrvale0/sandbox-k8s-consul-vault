#+seq_todo: next(n) todo(t) waiting(w) someday(s) | done(d) cancelled(c)

* next read up on the latest features in the Consul Helm Chart
  DEADLINE: <2018-11-20 Tue>
* next Consul single DC w/ Network Segments
  DEADLINE: <2018-11-21 Wed>
* next absolutely confirm the multi-DC stuff is not yet in place
* todo Consul in ASK
* todo Consul in EKS
