output "k8s_api_endpoint" {
       value = "${google_container_cluster.k8s.endpoint}"
}

output "k8s_master_auth_client_key" {
       value = "${google_container_cluster.k8s.master_auth.0.client_key}"
}

output "k8s_master_auth_client_certificate" {
   value = "${google_container_cluster.k8s.master_auth.0.client_certificate}"
}

output "k8s_master_auth_cluster_ca_certificate" {
   value = "${google_container_cluster.k8s.master_auth.0.cluster_ca_certificate}"
}
