variable "gcp_project" {}
variable "gcp_credentials" {}
variable "gcp_region" {}
variable "gcp_zone" {}
variable "initial_node_count" {}
variable "master_username" {}
variable "master_password" {}

variable "node_machine_type" { default = "n1-standard-2" }
variable "node_disk_size" { default = "20" }
