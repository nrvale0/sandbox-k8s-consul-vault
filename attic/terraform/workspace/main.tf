provider "tfe" {
  hostname = "${var.tfe_hostname}"
  token = "${var.tfe_api_token}"
}

resource "tfe_workspace" "workspace" {
  name = "${var.workspace_name}"
  organization = "${var.tfe_org_name}"
  auto_apply = "${var.workspace_auto_apply}"
  working_directory = "${var.workspace_working_directory}"

  vcs_repo {
    identifier = "${var.workspace_vcs_repo}"
    branch = "${var.workspace_branch}"
    oauth_token_id = "${var.workspace_oauth_token}"
  }
}

resource "tfe_variable" "CONFIRM_DESTROY" {
  key = "CONFIRM_DESTROY"
  value = "1"
  category = "env"
  workspace_id = "${tfe_workspace.workspace.id}"
}

resource "tfe_variable" "gcp_project" {
  key = "project"
  value = "${var.workspace_gcp_project}"
  category = "terraform"
  workspace_id = "${tfe_workspace.workspace.id}"
}

resource "tfe_variable" "gcp_zone" {
  key = "zone"
  value = "${var.workspace_gcp_zone}"
  category = "terraform"
  workspace_id = "${tfe_workspace.workspace.id}"
}

resource "tfe_variable" "gcp_credentials" {
  key = "GOOGLE_CREDENTIALS"
  value = "${trimspace(chomp(replace(var.workspace_gcp_credentials, "\n", "")))}"
  category = "env"
  workspace_id = "${tfe_workspace.workspace.id}"
  sensitive = true
}

resource "tfe_variable" "init_cli" {
  key = "init_cli"
  value = true
  category = "terraform"
  workspace_id = "${tfe_workspace.workspace.id}"
}
