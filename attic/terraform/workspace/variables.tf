variable "tfe_hostname" { default = "app.terraform.io" }
variable "tfe_api_token" {}
variable "tfe_org_name" {}

variable "workspace_name" { default = "sandbox-k8s-consul" }
variable "workspace_oauth_token" {}
variable "workspace_auto_apply" { default = true }
variable "workspace_vcs_repo" {}
variable "workspace_branch" { default = "master" }
variable "workspace_working_directory" { default = "test/terraform" }

variable "workspace_gcp_project" {}
variable "workspace_gcp_credentials" {}
variable "workspace_gcp_region" { default = "us-west1" }
variable "workspace_gcp_zone" { default = "us-west1-a" }
