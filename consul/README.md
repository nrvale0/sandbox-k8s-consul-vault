

# Summary

Sandbox for deploying Consul Enterprise via Helm Charts in various Reference Architecture scenarios.
Ex: single DC, multi-DC, single DC with network segments, etc.


# Resources

-   [official Consul Helm Chart](https://github.com/hashicorp/consul-helm)


# Usage

    unset LS_COLORS; export TERM=xterm; set +x; pyenv shell 2.7.13


## Google Cloud


### single DC

In the single DC and Google Cloud scenario we'll just leverage the Terraform provisioning code which is in the [Consul Helmn Chart repo](https://github.com/hashicorp/consul-helm/tree/master/test/terraform).

-   setting up the inputs

    Set some inputs via the env.sh file:
    
    export CONSUL<sub>CHART</sub><sub>URL</sub>="<https://github.com/hashicorp/consul-helm>"
    export CONSUL<sub>CHART</sub><sub>VERSION</sub>="v0.3.0"
    
    export GOOGLE<sub>APPLICATION</sub><sub>CREDENTIALS</sub>="\({HOME}/.config/gcloud/application_default_credentials.json"
         export TF_VAR_project="\)(gcloud config get-value project)"
    export TF<sub>VAR</sub><sub>init</sub><sub>cli</sub>="true"
    export region="us-central1"
    export TF<sub>VAR</sub><sub>zone</sub>="${region}-a"
    
        sourcey ./env.sh

-   download the Consul Helm chart repo

        git clone -q "${CONSUL_CHART_URL}" ./consul-helm
    
        (cd consul-helm && \
             git fetch --all && \
             git pull --tags && \
             git checkout "${CONSUL_CHART_VERSION}")
    
        
        > > Fetching origin
        Already up to date.
        Note: checking out 'v0.3.0'.
        
        You are in 'detached HEAD' state. You can look around, make experimental
        changes and commit them, and you can discard any commits you make in this
        state without impacting any branches by performing another checkout.
        
        If you want to create a new branch to retain commits you create, you may
        do so (now or later) by using -b with the checkout command again. Example:
        HEAD is now at cd18089 update CHANGELOG

-   provision infrastructure/k8s

        (cd consul-helm/test/terraform && \
             terraform init && \
             terraform plan && \
             terraform apply -auto-approve)
    
        
        > > 
        [0m[1mInitializing provider plugins...[0m
        - Checking for available provider plugins on https://releases.hashicorp.com...
        - Downloading plugin for provider "random" (2.0.0)...
        - Downloading plugin for provider "null" (1.0.0)...
        - Downloading plugin for provider "google" (1.19.1)...
        
        The following providers do not have any version constraints in configuration,
        so the latest version was installed.
        
        To prevent automatic upgrades to new major versions that may contain breaking
        changes, it is recommended to add version = "..." constraints to the
        corresponding provider blocks in configuration, with the constraint strings
        suggested below.
        1.19"
        1.0"
        2.0"
        
        [0m[1m[32mTerraform has been successfully initialized![0m[32m[0m
        [0m[32m
        You may now begin working with Terraform. Try running "terraform plan" to see
        any changes that are required for your infrastructure. All Terraform commands
        should now work.
        
        If you ever set or change modules or backend configuration for Terraform,
        rerun this command to reinitialize your working directory. If you forget, other
        commands will detect it and remind you to do so if necessary.[0m
        [0m[1mRefreshing Terraform state in-memory prior to plan...[0m
        The refreshed state will be used to calculate this plan, but will not be
        persisted to local or remote state storage.
        [0m
        [0m[1mdata.google_container_engine_versions.main: Refreshing state...[0m
        
        ------------------------------------------------------------------------
        
        An execution plan has been generated and is shown below.
        Resource actions are indicated with the following symbols:
          [32m+[0m create
        [0m
        Terraform will perform the following actions:
        
        [32m  [32m+[0m [32mgoogle_container_cluster.cluster
        
        :          <computed>
        :             <computed>
        enable_binary_authorization: "false"
              enable_kubernetes_alpha:     "false"
              enable_legacy_abac:          "true"
              enable_tpu:                  "false"
        initial_node_count:          "5"
        :       <computed>
        
        :               <computed>
        min_master_version:          "1.11.2-gke.18"
        
        {random_id.suffix.dec}"
              network:                     "default"
        :            <computed>
        :               <computed>
        :                 <computed>
              node_version:                "1.11.2-gke.18"
              private_cluster:             "false"
              project:                     "demos-nvalentine"
        zone:                        "us-central1-a"
        [0m
        [0m[32m  [32m+[0m [32mrandom_id.suffix
        
        
        
        byte_length:                 "4"
        
        [0m
        [0m
        [0m[1mPlan:[0m 2 to add, 0 to change, 0 to destroy.[0m
        
        ------------------------------------------------------------------------
        
        Note: You didn't specify an "-out" parameter to save this plan, so Terraform
        can't guarantee that exactly these actions will be performed if
        "terraform apply" is subsequently run.
        
        [0m[1mdata.google_container_engine_versions.main: Refreshing state...[0m
        [0m[1mrandom_id.suffix: Creating...[0m
        "<computed>"
        "<computed>"
        "<computed>"
        "4"
        "<computed>"
        "<computed>"[0m
        [0m[1mrandom_id.suffix: Creation complete after 0s (ID: lraw3Q)[0m[0m
        [0m[1mgoogle_container_cluster.cluster: Creating...[0m
        :          "" => "<computed>"
        :             "" => "<computed>"
        "<computed>"
        "false"
        "false"
        "true"
        "false"
        "<computed>"
        "5"
        :       "" => "<computed>"
        "<computed>"
        :               "" => "<computed>"
        "<computed>"
        "1.11.2-gke.18"
        "<computed>"
        "consul-k8s-2528555229"
        "default"
        :            "" => "<computed>"
        :               "" => "<computed>"
        :                 "" => "<computed>"
        "1.11.2-gke.18"
        "false"
        "demos-nvalentine"
        "<computed>"
        "us-central1-a"[0m
        [0m[1mgoogle_container_cluster.cluster: Still creating... (10s elapsed)[0m[0m
        [0m[1mgoogle_container_cluster.cluster: Still creating... (20s elapsed)[0m[0m
        [0m[1mgoogle_container_cluster.cluster: Still creating... (30s elapsed)[0m[0m
        [0m[1mgoogle_container_cluster.cluster: Still creating... (40s elapsed)[0m[0m
        [0m[1mgoogle_container_cluster.cluster: Still creating... (50s elapsed)[0m[0m
        [0m[1mgoogle_container_cluster.cluster: Still creating... (1m0s elapsed)[0m[0m
        [0m[1mgoogle_container_cluster.cluster: Still creating... (1m10s elapsed)[0m[0m
        [0m[1mgoogle_container_cluster.cluster: Still creating... (1m20s elapsed)[0m[0m
        [0m[1mgoogle_container_cluster.cluster: Still creating... (1m30s elapsed)[0m[0m
        [0m[1mgoogle_container_cluster.cluster: Still creating... (1m40s elapsed)[0m[0m
        [0m[1mgoogle_container_cluster.cluster: Still creating... (1m50s elapsed)[0m[0m
        [0m[1mgoogle_container_cluster.cluster: Still creating... (2m0s elapsed)[0m[0m
        [0m[1mgoogle_container_cluster.cluster: Still creating... (2m10s elapsed)[0m[0m
        [0m[1mgoogle_container_cluster.cluster: Still creating... (2m20s elapsed)[0m[0m
        [0m[1mgoogle_container_cluster.cluster: Still creating... (2m30s elapsed)[0m[0m
        [0m[1mgoogle_container_cluster.cluster: Still creating... (2m40s elapsed)[0m[0m
        [0m[1mgoogle_container_cluster.cluster: Still creating... (2m50s elapsed)[0m[0m
        [0m[1mgoogle_container_cluster.cluster: Still creating... (3m0s elapsed)[0m[0m
        [0m[1mgoogle_container_cluster.cluster: Still creating... (3m10s elapsed)[0m[0m
        [0m[1mgoogle_container_cluster.cluster: Still creating... (3m20s elapsed)[0m[0m
        [0m[1mgoogle_container_cluster.cluster: Creation complete after 3m25s (ID: consul-k8s-2528555229)[0m[0m
        [0m[1m[32m
        Apply complete! Resources: 2 added, 0 changed, 0 destroyed.[0m
        [0m[1m[32m
        Outputs:
        
        cluster_id = consul-k8s-2528555229
        cluster_name = consul-k8s-2528555229[0m

-   get credentials for the new k8s cluster

        cluster_name="$(terraform state show --state=./consul-helm/test/terraform/terraform.tfstate google_container_cluster.cluster | egrep '^name ' | cut -f2 -d'=' | xargs)"
        echo "Getting credentials from cluster-name ${cluster_name}..."
        gcloud container clusters get-credentials --region="${TF_VAR_zone}" "${cluster_name}"
    
        
        Getting credentials from cluster-name consul-k8s-2528555229...
        Fetching cluster endpoint and auth data.
        kubeconfig entry generated for consul-k8s-2528555229.
    
        kubectl version && \
            kubectl get pods --namespace=kube-system
    
        
        Client Version: version.Info{Major:"1", Minor:"12", GitVersion:"v1.12.0", GitCommit:"0ed33881dc4355495f623c6f22e7dd0b7632b7c0", GitTreeState:"clean", BuildDate:"2018-09-27T17:05:32Z", GoVersion:"go1.10.4", Compiler:"gc", Platform:"linux/amd64"}
        Server Version: version.Info{Major:"1", Minor:"11+", GitVersion:"v1.11.2-gke.18", GitCommit:"5796233393d7bc034428de15191ad3d2eaff95fb", GitTreeState:"clean", BuildDate:"2018-11-08T20:49:08Z", GoVersion:"go1.10.3b4", Compiler:"gc", Platform:"linux/amd64"}
        NAME                                                             READY   STATUS    RESTARTS   AGE
        event-exporter-v0.2.1-7978ddf677-9bzdn                           2/2     Running   0          1h
        fluentd-gcp-scaler-5d85d4b48b-8ctbb                              1/1     Running   0          1h
        fluentd-gcp-v3.1.0-gd7ts                                         2/2     Running   0          1h
        fluentd-gcp-v3.1.0-nk9lk                                         2/2     Running   0          1h
        fluentd-gcp-v3.1.0-rgtsd                                         2/2     Running   0          1h
        fluentd-gcp-v3.1.0-vgjw8                                         2/2     Running   0          1h
        fluentd-gcp-v3.1.0-zpsg2                                         2/2     Running   0          1h
        heapster-v1.6.0-beta.1-794bc8d85f-42ptc                          3/3     Running   0          1h
        kube-dns-548976df6c-qnfml                                        4/4     Running   0          1h
        kube-dns-548976df6c-xpfhh                                        4/4     Running   0          1h
        kube-dns-autoscaler-67c97c87fb-brxzg                             1/1     Running   0          1h
        kube-proxy-gke-consul-k8s-252855522-default-pool-deb2ed5c-7td5   1/1     Running   0          1h
        kube-proxy-gke-consul-k8s-252855522-default-pool-deb2ed5c-cjdg   1/1     Running   0          1h
        kube-proxy-gke-consul-k8s-252855522-default-pool-deb2ed5c-pzfh   1/1     Running   0          1h
        kube-proxy-gke-consul-k8s-252855522-default-pool-deb2ed5c-rn15   1/1     Running   0          1h
        kube-proxy-gke-consul-k8s-252855522-default-pool-deb2ed5c-v2pd   1/1     Running   0          1h
        l7-default-backend-5bc54cfb57-lsjb2                              1/1     Running   0          1h
        metrics-server-v0.2.1-fd596d746-mgrwx                            2/2     Running   0          1h

-   provision Consul

        echo "FIXME: provision Consul cluster here."
    
        FIXME: provision Consul cluster here.


### multi-DC

-   provision infrastructure/k8s

-   provision Consul

