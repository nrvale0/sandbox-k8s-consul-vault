#+title: Consul Enterprise on k8s
#+options: num:nil toc:nil
#+property: header-args:sh :session consul-helm :results output :exports both

* Summary

Sandbox for deploying Consul Enterprise via Helm Charts in various Reference Architecture scenarios.
Ex: single DC, multi-DC, single DC with network segments, etc.

* Resources
  - [[https://github.com/hashicorp/consul-helm][official Consul Helm Chart]]

* Usage

  #+begin_src sh :session consul-helm :results none :exports none
  unset LS_COLORS; export TERM=xterm; set +x; pyenv shell 2.7.13
  #+end_src

** Google Cloud
*** single DC

    In the single DC and Google Cloud scenario we'll just leverage the Terraform provisioning code which is in the [[https://github.com/hashicorp/consul-helm/tree/master/test/terraform][Consul Helmn Chart repo]].

**** setting up the inputs

     Set some inputs via the env.sh file:

     #+include: "./env.sh"

     #+begin_src sh
       source ./env.sh
     #+end_src

     #+RESULTS:

**** download the Consul Helm chart repo
     #+begin_src sh :prologue rm -rf consul-helm
       git clone -q "${CONSUL_CHART_URL}" ./consul-helm
     #+end_src

     #+RESULTS:

     #+begin_src sh
       (cd consul-helm && \
	    git fetch --all && \
	    git pull --tags && \
	    git checkout "${CONSUL_CHART_VERSION}")
     #+end_src

     #+RESULTS:
     #+begin_example

     > > Fetching origin
     Already up to date.
     Note: checking out 'v0.3.0'.

     You are in 'detached HEAD' state. You can look around, make experimental
     changes and commit them, and you can discard any commits you make in this
     state without impacting any branches by performing another checkout.

     If you want to create a new branch to retain commits you create, you may
     do so (now or later) by using -b with the checkout command again. Example:
     HEAD is now at cd18089 update CHANGELOG
     #+end_example

**** provision infrastructure/k8s

    #+begin_src sh
      (cd consul-helm/test/terraform && \
	   terraform init && \
	   terraform plan && \
	   terraform apply -auto-approve)
    #+end_src

    #+RESULTS:
    #+begin_example

    > > 
    [0m[1mInitializing provider plugins...[0m
    - Checking for available provider plugins on https://releases.hashicorp.com...
    - Downloading plugin for provider "null" (1.0.0)...
    - Downloading plugin for provider "google" (1.19.1)...
    - Downloading plugin for provider "random" (2.0.0)...

    The following providers do not have any version constraints in configuration,
    so the latest version was installed.

    To prevent automatic upgrades to new major versions that may contain breaking
    changes, it is recommended to add version = "..." constraints to the
    corresponding provider blocks in configuration, with the constraint strings
    suggested below.
    1.19"
    1.0"
    2.0"

    [0m[1m[32mTerraform has been successfully initialized![0m[32m[0m
    [0m[32m
    You may now begin working with Terraform. Try running "terraform plan" to see
    any changes that are required for your infrastructure. All Terraform commands
    should now work.

    If you ever set or change modules or backend configuration for Terraform,
    rerun this command to reinitialize your working directory. If you forget, other
    commands will detect it and remind you to do so if necessary.[0m
    [0m[1mRefreshing Terraform state in-memory prior to plan...[0m
    The refreshed state will be used to calculate this plan, but will not be
    persisted to local or remote state storage.
    [0m
    [0m[1mdata.google_container_engine_versions.main: Refreshing state...[0m

    ------------------------------------------------------------------------

    An execution plan has been generated and is shown below.
    Resource actions are indicated with the following symbols:
      [32m+[0m create
    [0m
    Terraform will perform the following actions:

    [32m  [32m+[0m [32mgoogle_container_cluster.cluster

    :          <computed>
    :             <computed>
    enable_binary_authorization: "false"
	  enable_kubernetes_alpha:     "false"
	  enable_legacy_abac:          "true"
	  enable_tpu:                  "false"
    initial_node_count:          "5"
    :       <computed>

    :               <computed>
    min_master_version:          "1.11.2-gke.18"

    {random_id.suffix.dec}"
	  network:                     "default"
    :            <computed>
    :               <computed>
    :                 <computed>
	  node_version:                "1.11.2-gke.18"
	  private_cluster:             "false"
	  project:                     "demos-nvalentine"
    zone:                        "us-central1-a"
    [0m
    [0m[32m  [32m+[0m [32mnull_resource.helm

    :                  <computed>
    [0m
    [0m[32m  [32m+[0m [32mnull_resource.kubectl

    :                  <computed>
    [0m
    [0m[32m  [32m+[0m [32mrandom_id.suffix



    byte_length:                 "4"

    [0m
    [0m
    [0m[1mPlan:[0m 4 to add, 0 to change, 0 to destroy.[0m

    ------------------------------------------------------------------------

    Note: You didn't specify an "-out" parameter to save this plan, so Terraform
    can't guarantee that exactly these actions will be performed if
    "terraform apply" is subsequently run.

    [0m[1mdata.google_container_engine_versions.main: Refreshing state...[0m
    [0m[1mrandom_id.suffix: Creating...[0m
    "<computed>"
    "<computed>"
    "<computed>"
    "4"
    "<computed>"
    "<computed>"[0m
    [0m[1mrandom_id.suffix: Creation complete after 0s (ID: h2qwcA)[0m[0m
    [0m[1mgoogle_container_cluster.cluster: Creating...[0m
    :          "" => "<computed>"
    :             "" => "<computed>"
    "<computed>"
    "false"
    "false"
    "true"
    "false"
    "<computed>"
    "5"
    :       "" => "<computed>"
    "<computed>"
    :               "" => "<computed>"
    "<computed>"
    "1.11.2-gke.18"
    "<computed>"
    "consul-k8s-2271916144"
    "default"
    :            "" => "<computed>"
    :               "" => "<computed>"
    :                 "" => "<computed>"
    "1.11.2-gke.18"
    "false"
    "demos-nvalentine"
    "<computed>"
    "us-central1-a"[0m
    [0m[1mgoogle_container_cluster.cluster: Still creating... (10s elapsed)[0m[0m
    [0m[1mgoogle_container_cluster.cluster: Still creating... (20s elapsed)[0m[0m
    [0m[1mgoogle_container_cluster.cluster: Still creating... (30s elapsed)[0m[0m
    [0m[1mgoogle_container_cluster.cluster: Still creating... (40s elapsed)[0m[0m
    [0m[1mgoogle_container_cluster.cluster: Still creating... (50s elapsed)[0m[0m
    [0m[1mgoogle_container_cluster.cluster: Still creating... (1m0s elapsed)[0m[0m
    [0m[1mgoogle_container_cluster.cluster: Still creating... (1m10s elapsed)[0m[0m
    [0m[1mgoogle_container_cluster.cluster: Still creating... (1m20s elapsed)[0m[0m
    [0m[1mgoogle_container_cluster.cluster: Still creating... (1m30s elapsed)[0m[0m
    [0m[1mgoogle_container_cluster.cluster: Still creating... (1m40s elapsed)[0m[0m
    [0m[1mgoogle_container_cluster.cluster: Still creating... (1m50s elapsed)[0m[0m
    [0m[1mgoogle_container_cluster.cluster: Still creating... (2m0s elapsed)[0m[0m
    [0m[1mgoogle_container_cluster.cluster: Still creating... (2m10s elapsed)[0m[0m
    [0m[1mgoogle_container_cluster.cluster: Still creating... (2m20s elapsed)[0m[0m
    [0m[1mgoogle_container_cluster.cluster: Still creating... (2m30s elapsed)[0m[0m
    [0m[1mgoogle_container_cluster.cluster: Still creating... (2m40s elapsed)[0m[0m
    [0m[1mgoogle_container_cluster.cluster: Still creating... (2m50s elapsed)[0m[0m
    [0m[1mgoogle_container_cluster.cluster: Still creating... (3m0s elapsed)[0m[0m
    [0m[1mgoogle_container_cluster.cluster: Still creating... (3m10s elapsed)[0m[0m
    [0m[1mgoogle_container_cluster.cluster: Still creating... (3m20s elapsed)[0m[0m
    [0m[1mgoogle_container_cluster.cluster: Creation complete after 3m26s (ID: consul-k8s-2271916144)[0m[0m
    [0m[1mnull_resource.kubectl: Creating...[0m
    :       "" => "1"
    "consul-k8s-2271916144"[0m
    [0m[1mnull_resource.kubectl: Provisioning with 'local-exec'...[0m[0m
    [0m[0mnull_resource.kubectl (local-exec): Executing: ["/bin/sh" "-c" "gcloud container clusters get-credentials --zone=us-central1-a consul-k8s-2271916144"]
    [0m[0mnull_resource.kubectl (local-exec): Fetching cluster endpoint and auth data.
    [0m[0mnull_resource.kubectl (local-exec): kubeconfig entry generated for consul-k8s-2271916144.
    [0m[1mnull_resource.kubectl: Creation complete after 1s (ID: 4502163301081985488)[0m[0m
    [0m[1mnull_resource.helm: Creating...[0m
    :       "" => "1"
    "consul-k8s-2271916144"[0m
    [0m[1mnull_resource.helm: Provisioning with 'local-exec'...[0m[0m
    [0m[0mnull_resource.helm (local-exec): Executing: ["/bin/sh" "-c" "kubectl apply -f '/home/nrvale0/workspace/gitlab/nrvale0/sandbox-k8s-consul-vault/consul/consul-helm/test/terraform/service-account.yaml'\nhelm init --service-account helm\n"]
    [0m[0mnull_resource.helm (local-exec): serviceaccount/helm created
    [0m[0mnull_resource.helm (local-exec): clusterrolebinding.rbac.authorization.k8s.io/helm created
    HELM_HOME has been configured at /home/nrvale0/.helm.
    [0m[0m
    [0m[0mnull_resource.helm (local-exec): Tiller (the Helm server-side component) has been installed into your Kubernetes Cluster.
    [0m[0m
    [0m[0mnull_resource.helm (local-exec): Please note: by default, Tiller is deployed with an insecure 'allow unauthenticated users' policy.
    [0m[0mnull_resource.helm (local-exec): To prevent this, run `helm init` with the --tiller-tls-verify flag.
    securing-your-helm-installation
    [0m[0mnull_resource.helm (local-exec): Happy Helming!
    [0m[1mnull_resource.helm: Creation complete after 4s (ID: 2891043764975058730)[0m[0m
    [0m[1m[32m
    Apply complete! Resources: 4 added, 0 changed, 0 destroyed.[0m
    [0m[1m[32m
    Outputs:

    cluster_id = consul-k8s-2271916144
    cluster_name = consul-k8s-2271916144[0m
    #+end_example

**** get credentials for the new k8s cluster
     #+begin_src sh
       cluster_name="$(terraform state show --state=./consul-helm/test/terraform/terraform.tfstate google_container_cluster.cluster | egrep '^name ' | cut -f2 -d'=' | xargs)"
       echo "Getting credentials from cluster-name ${cluster_name}..."
       gcloud container clusters get-credentials --region="${TF_VAR_zone}" "${cluster_name}"
     #+end_src

     #+RESULTS:
     : 
     : Getting credentials from cluster-name consul-k8s-2271916144...
     : Fetching cluster endpoint and auth data.
     : kubeconfig entry generated for consul-k8s-2271916144.

     #+begin_src sh
       kubectl version && \
	   kubectl get pods --namespace=kube-system
     #+end_src

     #+RESULTS:
     #+begin_example

     Client Version: version.Info{Major:"1", Minor:"12", GitVersion:"v1.12.0", GitCommit:"0ed33881dc4355495f623c6f22e7dd0b7632b7c0", GitTreeState:"clean", BuildDate:"2018-09-27T17:05:32Z", GoVersion:"go1.10.4", Compiler:"gc", Platform:"linux/amd64"}
     Server Version: version.Info{Major:"1", Minor:"11+", GitVersion:"v1.11.2-gke.18", GitCommit:"5796233393d7bc034428de15191ad3d2eaff95fb", GitTreeState:"clean", BuildDate:"2018-11-08T20:49:08Z", GoVersion:"go1.10.3b4", Compiler:"gc", Platform:"linux/amd64"}
     NAME                                                             READY   STATUS              RESTARTS   AGE
     event-exporter-v0.2.1-7978ddf677-lz8v9                           0/2     ContainerCreating   0          35s
     fluentd-gcp-scaler-5d85d4b48b-7dmx4                              0/1     ContainerCreating   0          24s
     fluentd-gcp-v3.1.0-8m8n9                                         0/2     ContainerCreating   0          35s
     fluentd-gcp-v3.1.0-mhjtx                                         0/2     ContainerCreating   0          35s
     fluentd-gcp-v3.1.0-nzbq5                                         0/2     ContainerCreating   0          35s
     fluentd-gcp-v3.1.0-r5kr7                                         0/2     ContainerCreating   0          35s
     fluentd-gcp-v3.1.0-z6z7x                                         0/2     ContainerCreating   0          35s
     heapster-v1.6.0-beta.1-8688fdf95-p8r48                           0/3     ContainerCreating   0          9s
     heapster-v1.6.0-beta.1-f75df7969-rc4rf                           0/3     Terminating         0          36s
     kube-dns-548976df6c-hqvbz                                        0/4     ContainerCreating   0          6s
     kube-dns-548976df6c-rdbwj                                        0/4     ContainerCreating   0          35s
     kube-dns-autoscaler-67c97c87fb-66rdw                             1/1     Running             0          14s
     kube-proxy-gke-consul-k8s-227191614-default-pool-72f8e727-cf7v   1/1     Running             0          28s
     kube-proxy-gke-consul-k8s-227191614-default-pool-72f8e727-gcx9   1/1     Running             0          28s
     kube-proxy-gke-consul-k8s-227191614-default-pool-72f8e727-nvd6   1/1     Running             0          33s
     kube-proxy-gke-consul-k8s-227191614-default-pool-72f8e727-vkc5   1/1     Running             0          29s
     kube-proxy-gke-consul-k8s-227191614-default-pool-72f8e727-xql5   1/1     Running             0          33s
     l7-default-backend-5bc54cfb57-4hp4k                              0/1     ContainerCreating   0          36s
     metrics-server-v0.2.1-597c89dc98-8fj2r                           2/2     Terminating         0          33s
     metrics-server-v0.2.1-fd596d746-rhzb7                            0/2     ContainerCreating   0          11s
     tiller-deploy-f5d5d4d4b-96r6n                                    0/1     ContainerCreating   0          2s
     #+end_example

**** provision Consul
     #+begin_src sh
       helm install --wait consul-helm
     #+end_src

     #+RESULTS:
     #+begin_example
     NAME:   giggly-bison
     LAST DEPLOYED: Tue Nov 20 15:08:53 2018
     NAMESPACE: default
     STATUS: DEPLOYED

     RESOURCES:
     v1/Pod(related)
     NAME                          READY  STATUS   RESTARTS  AGE
     giggly-bison-consul-server-0  1/1    Running  0         51s
     giggly-bison-consul-server-1  1/1    Running  0         51s
     giggly-bison-consul-server-2  1/1    Running  0         51s
     v1/ConfigMap

     NAME                               AGE
     giggly-bison-consul-client-config  51s
     giggly-bison-consul-server-config  51s
     v1/Service
     giggly-bison-consul-dns     51s
     giggly-bison-consul-server  51s
     giggly-bison-consul-ui      51s
     v1/DaemonSet
     giggly-bison-consul  51s
     v1/StatefulSet
     giggly-bison-consul-server  51s
     v1beta1/PodDisruptionBudget
     giggly-bison-consul-server  51s
     #+end_example


*** single DC with Consul Enterprise Network Segments

*** multi-DC

    Deployments spanning multiple data centers (and thus multiple k8s clusters) is not yet supported in v0.3.0 of the official Consul Helm Chart.
