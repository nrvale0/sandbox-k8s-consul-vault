export CONSUL_CHART_URL="https://github.com/hashicorp/consul-helm"
export CONSUL_CHART_VERSION="v0.3.0"

export GOOGLE_APPLICATION_CREDENTIALS="${HOME}/.config/gcloud/application_default_credentials.json"
export TF_VAR_project="$(gcloud config get-value project)"
export TF_VAR_init_cli="true"
export region="us-central1"
export TF_VAR_zone="${region}-a"
